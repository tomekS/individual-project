import DataLoaders.EmployeesLoader;
import Employees.Employee;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class EmployeesLoaderTests {
    @Test
    public void should_parse_fulltimeemployee_json(){
        String json = "{\"name\":\"tomek s\",\"id\":1234}";
        Employee fromJSON = EmployeesLoader.getFullTimeEmployeeFromJSON(json);
        assertEquals("tomek s", fromJSON.getName());
        assertEquals(1234, fromJSON.getId());
        System.out.println(fromJSON);
    }

}
