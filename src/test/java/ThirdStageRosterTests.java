import Roster.ThirdStageRoster;
import Shifts.Shift;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
//TODO dopisac wiecej testow i przetestowac
public class ThirdStageRosterTests {
    @Test
    public void shifts_are_closer_than_11hurs(){
        Shift first = new Shift( LocalDateTime.of(LocalDate.now(), LocalTime.of(6,0)),
                LocalDateTime.of(LocalDate.now(), LocalTime.of(14,0)), 1);

        Shift second = new Shift( LocalDateTime.of(LocalDate.now(), LocalTime.of(14,0)),
                LocalDateTime.of(LocalDate.now(), LocalTime.of(22,0)), 1);

        boolean shiftsAreCloserThan11Hours = ThirdStageRoster.shiftsAreCloserThan11Hours(first, second);

        Assert.assertTrue(shiftsAreCloserThan11Hours);
    }

    @Test
    public void duration_beetwen_shifts_equals_11_hours(){
        Shift first = new Shift( LocalDateTime.of(LocalDate.now(), LocalTime.of(6,0)),
                LocalDateTime.of(LocalDate.now(), LocalTime.of(14,0)), 1);

        Shift second = new Shift( LocalDateTime.of(LocalDate.now(), LocalTime.of(14,0)).plusHours(11),
                LocalDateTime.of(LocalDate.now(), LocalTime.of(22,0)).plusHours(11), 1);

        boolean shiftsAreCloserThan11Hours = ThirdStageRoster.shiftsAreCloserThan11Hours(first, second);

        Assert.assertFalse(shiftsAreCloserThan11Hours);
    }
}
