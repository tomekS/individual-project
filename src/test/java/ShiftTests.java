import Development.DataSupplier;
import Employees.BasicEmployee;
import Employees.Employee;
import Roster.Roster;
import Shifts.Shift;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ShiftTests {
    @Test
    public void no_references_to_same_employees_list_should_be_copied() {
        //setup
        Shift shiftToCopy = new Shift(LocalDateTime.now().withHour(7).withMinute(0),
                LocalDateTime.now().withHour(19).withMinute(0), 3);
        shiftToCopy.addEmployee(new BasicEmployee("testowy", 1, 1.0));
        shiftToCopy.addEmployee(new BasicEmployee("testowy2", 2, 1.0));
        shiftToCopy.addEmployee(new BasicEmployee("testowy3", 3, 1.0));
        Shift copied = shiftToCopy.copy();
        //examine


    }

    @Test
    public void test_copying_using_stream() {
        //setup
        Set<Shift> shiftsToCopy = DataSupplier.getTestShifts();
        //examine
        Employee employee = DataSupplier.getTestEmployees().stream()
                .findAny()
                .orElse(new BasicEmployee("test", 1, 40));

        Shift shift = shiftsToCopy.stream()
                .findFirst()
                .orElse(null);

        Set<Shift> copies = shiftsToCopy.stream()
                .map(Shift::copy)
                .collect(Collectors.toSet());

        shift.addEmployee(employee);
        //verify
        Assert.assertTrue(shiftsToCopy.stream()
                .filter(shift1 -> shift1.getEmployeeList().size() == 1)
                .count() == 1);

        Assert.assertTrue(copies.stream().filter(shift1 -> shift1.getEmployeeList().size() != 0).count() == 0);

        System.out.println(copies);
        System.out.println(shiftsToCopy);
    }

    @Test
    public void equals_method_test_return_true_for_same_rosters(){
        //setup
        Set<Shift> shifts_1 = DataSupplier.getTestShifts();
        Set<Shift> shifts_2 = DataSupplier.getTestShifts();
        Set<Employee> employees = DataSupplier.getTestEmployees();
        //examine
        for (Shift shift : shifts_1) {
            for (Employee employee : employees) {
                shift.addEmployee(employee);
            }
        }

        for (Shift shift : shifts_2) {
            for (Employee employee : employees) {
                shift.addEmployee(employee);
            }
        }
        //verify
        Roster roster_1 = new Roster(shifts_1, employees);
        Roster roster_2 = new Roster(shifts_2, employees);
        Assert.assertTrue(roster_1.equals(roster_2));
    }

    @Test
    public void equals_method_test_return_false_for_different_rosters(){
        //setup
        Set<Shift> shifts_1 = DataSupplier.getTestShifts();
        Set<Shift> shifts_2 = DataSupplier.getTestShifts();
        Set<Employee> employees = DataSupplier.getTestEmployees();
        //examine
        for (Shift shift : shifts_1) {
            for (Employee employee : employees) {
                shift.addEmployee(employee);
            }
        }

        for (Shift shift : shifts_2) {
            for (Employee employee : employees) {
                shift.addEmployee(employee);
            }
        }

        shifts_1.stream().findFirst().get().removeEmployee(1);

        //verify
        System.out.println("dupa");
        Roster roster_1 = new Roster(shifts_1, employees);
        Roster roster_2 = new Roster(shifts_2, employees);
        Assert.assertFalse(roster_1.equals(roster_2));
    }


}
