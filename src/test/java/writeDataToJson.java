import Development.DataSupplier;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class writeDataToJson {
    public static void main(String[] args) throws IOException {
        try (Writer writer = new FileWriter("ocado_employees.json")) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(DataSupplier.getOcadoTestEmployees(), writer);
        }
        try (Writer writer = new FileWriter("ocado_shifts.json")) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(DataSupplier.getOcadoShifts(), writer);
        }
    }
}
