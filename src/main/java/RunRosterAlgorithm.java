import Builders.MultiThreadEngine;
import CSVReader.EmployeeCSVReader;
import CSVReader.PreferencesCSVReader;
import CSVReader.RequirementCSVReader;
import Employees.Employee;
import Preferences.Preference;
import Shifts.Shift;
import Writers.ErrorWriter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

public class RunRosterAlgorithm {
    public static void main(String[] args) throws IOException {
        Path workingDirectory = Paths.get(args[3]);
        Path errorFilePath = Paths.get(String.valueOf(workingDirectory), "/errors.txt");
        File errorFile = new File(String.valueOf(errorFilePath));
        File tmp = new File(workingDirectory.toString() + File.separator + "tmp");
        File best = new File(workingDirectory.toString() + File.separator + "best");

        tmp.mkdir();
        errorFile.createNewFile();
        best.mkdir();

        Set<Employee> employees = new HashSet<>();
        Set<Shift> shifts = new HashSet<>();
        Set<Preference> preferences = new HashSet<>();

        try {
            employees = new HashSet<>(EmployeeCSVReader.readEmployeesFromFile(args[0]));
        } catch (Exception e) {
            ErrorWriter.writeErrorMessage(errorFile, e.getMessage());
        }

        try {
            shifts = new HashSet<>(RequirementCSVReader.readRequirementsFromFile(args[1]));
        } catch (Exception e) {
            ErrorWriter.writeErrorMessage(errorFile, e.getMessage());
        }

        try {
            preferences = new HashSet<>(PreferencesCSVReader.readPreferencesFromFile(args[2]));
        } catch (Exception e) {
            ErrorWriter.writeErrorMessage(errorFile, e.getMessage());
        }

        int numberOfThreads = Integer.parseInt(args[4]);
        int sizeOfPopulation = Integer.parseInt(args[5]);


        MultiThreadEngine engine = new MultiThreadEngine(employees, shifts, preferences, numberOfThreads,
                sizeOfPopulation, workingDirectory);

        try {
            engine.generate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
