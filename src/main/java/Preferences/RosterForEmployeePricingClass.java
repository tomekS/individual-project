package Preferences;

public class RosterForEmployeePricingClass {
    private static int workingSundayFine = 3;
    private static int workingSaturdayFine = 2;
    private static int workingNightFine = 1;
    private static int workingWeekendFine = 5;
    private static int oneFreeDayInRowFine = 100;

    public static int getOneFreeDayInRowFine() {
        return oneFreeDayInRowFine;
    }

    public static void setOneFreeDayInRowFine(int oneFreeDayInRowFine) {
        RosterForEmployeePricingClass.oneFreeDayInRowFine = oneFreeDayInRowFine;
    }

    public static int getWorkingSundayFine() {
        return workingSundayFine;
    }

    public static void setWorkingSundayFine(int workingSundayFine) {
        RosterForEmployeePricingClass.workingSundayFine = workingSundayFine;
    }

    public static int getWorkingSaturdayFine() {
        return workingSaturdayFine;
    }

    public static void setWorkingSaturdayFine(int workingSaturdayFine) {
        RosterForEmployeePricingClass.workingSaturdayFine = workingSaturdayFine;
    }

    public static int getWorkingNightFine() {
        return workingNightFine;
    }

    public static void setWorkingNightFine(int workingNightFine) {
        RosterForEmployeePricingClass.workingNightFine = workingNightFine;
    }

    public static int getWorkingWeekendFine() {
        return workingWeekendFine;
    }

    public static void setWorkingWeekendFine(int workingWeekendFine) {
        RosterForEmployeePricingClass.workingWeekendFine = workingWeekendFine;
    }
}
