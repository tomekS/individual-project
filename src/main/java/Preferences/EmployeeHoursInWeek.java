package Preferences;

import Employees.Employee;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class EmployeeHoursInWeek {
    private static Map<Integer, Map<Employee, Integer>> weekEmployeeHoursMap = new HashMap<>();

    public static int getNumbersOfHOursForEmployeInWeek(Employee employee, int week){
        if(weekEmployeeHoursMap.keySet().contains(week)){
            return weekEmployeeHoursMap.get(week).get(employee);
        }else{
            return 0;
        }

    }

    public static void fulFillMap(Set<Employee>employees, Set<Preference>preferences){
        Set<Integer> weeks = preferences.stream()
                .map(Preference::getWeekNumber)
                .distinct()
                .collect(Collectors.toSet());

        for (Integer week : weeks) {
            Map<Employee, Integer> map = new HashMap<>();
            for (Employee employee : employees) {
                int freeHoursInWeek = preferences.stream()
                        .filter(p -> p.getEmployeeId() == employee.getId())
                        .map(p -> 8)
                        .mapToInt(i -> i)
                        .sum();
                map.put(employee, freeHoursInWeek);
            }
            weekEmployeeHoursMap.put(week, map);
        }
    }
}
