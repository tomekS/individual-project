package Preferences;

import java.time.LocalDateTime;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class Preference implements Comparable<Preference> {

    private int employeeId;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

    public Preference(int employeeId, LocalDateTime startTime, LocalDateTime endTime) {
        this.employeeId = employeeId;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    @Override
    public int compareTo(Preference preference) {
        return this.startTime.compareTo(preference.getStartTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Preference that = (Preference) o;

        if (employeeId != that.employeeId) return false;
        if (!startTime.equals(that.startTime)) return false;
        return endTime.equals(that.endTime);
    }

    @Override
    public int hashCode() {
        int result = employeeId;
        result = 31 * result + startTime.hashCode();
        result = 31 * result + endTime.hashCode();
        return result;
    }

    public int getWeekNumber() {
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        return this.startTime.get(weekFields.weekOfWeekBasedYear());
    }
}
