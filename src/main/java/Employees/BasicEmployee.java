package Employees;

public class BasicEmployee implements Employee {

    private String name;
    private int id;
    private double jobTime;

    public BasicEmployee(String name, int id, double jobTime) {
        this.name = name;
        this.id = id;
        this.jobTime = jobTime;
    }

    @Override
    public int compareTo(Employee employee) {
        return this.id - employee.getId();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public double getJobTime() {
        return jobTime;
    }

    @Override
    public String toString() {
        return "BasicEmployee{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", jobTime=" + jobTime +
                '}';
    }
}
