package Employees;

public interface Employee extends Comparable<Employee>{
    public String getName();
    public int getId();
    public double getJobTime();
}
