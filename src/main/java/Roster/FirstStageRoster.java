package Roster;

import Employees.Employee;
import Shifts.Shift;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class FirstStageRoster {
    private Set<Employee> employees;
    private Set<Shift> shifts;

    public FirstStageRoster(Set<Employee> employees, Set<Shift> shifts) {
        this.employees = employees;
        this.shifts = shifts;
        generate();
    }

    private void generate() {
        for (Shift shift : shifts) {
            employees.forEach(shift::addEmployee);
        }
    }

    public Set<Shift> getShifts(){
        return Collections.unmodifiableSet(shifts);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(Shift shift: shifts){
            builder.append("_______________________\n");
            builder.append(shift.toString()).append('\n');
        }
        return builder.toString();
    }
}
