package Roster;

import Preferences.Preference;
import Shifts.Shift;

import java.util.Collections;
import java.util.Set;

public class SecondStageRoster {

    private Set<Shift> shiftSet;
    private Set<Preference> preferenceSet;

    public SecondStageRoster(FirstStageRoster firstStageRoster, Set<Preference> preferences) {
        this.shiftSet = firstStageRoster.getShifts();
        this.preferenceSet = preferences;
        generate();
    }

    private void generate() {
        preferenceSet.forEach(this::removeEmployeeFromShiftUsingPreference);
    }

    private void removeEmployeeFromShiftUsingPreference(Preference preference) {
        shiftSet.stream()
                .filter(shift -> preferenceIsInConflictWithShift(shift, preference))
                .forEach(shift -> shift.removeEmployee(preference.getEmployeeId()));
    }

    private boolean preferenceIsInConflictWithShift(Shift shift, Preference preference) {
        return (!preference.getStartTime().isBefore(shift.getStartTime()) && preference.getStartTime().isBefore(shift.getEndTime())) ||
                (preference.getEndTime().isAfter(shift.getStartTime()) && !preference.getEndTime().isAfter(shift.getEndTime()));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Shift shift : shiftSet) {
            builder.append("_______________________\n");
            builder.append(shift.toString()).append('\n');
        }
        return builder.toString();
    }

    public Set<Shift> getShiftSet(){
        return Collections.unmodifiableSet(shiftSet);
    }

}
