package Roster;

import Employees.Employee;
import Preferences.EmployeeHoursInWeek;
import Shifts.Shift;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class ThirdStageRoster {

    private Set<Shift> shiftSet;
    private Set<Shift> approvedShifts = new TreeSet<>();
    private TreeMap<Employee, Integer> employeesHoursMap = new TreeMap<>();

    public ThirdStageRoster(Set<Shift> shiftSet) {
        this.shiftSet = shiftSet;
        generate();
    }

    private void generate() {
//        System.out.println("generate");

        findAndApproveShiftsWithFactorLessOrEqualToZero();
        while (checkIfItIsPossibleToRemoveEmployeesFromShifts()) {
//            System.out.println("Thrid stage");
            // 1. Find shifts to remove employees from.
            // 2. Sort shift by te rate: "number of employees for shift - required number of employees".
            // 3. Get first shift

            List<Shift> shiftsToShuffle = shiftSet.stream()
                    .filter(shift -> !approvedShifts.contains(shift))
                    .filter(shift -> shift.getFactorRequiredAvailable() > 0)
                    .collect(Collectors.toList());

            Collections.shuffle(shiftsToShuffle);

            Shift shiftToRemoveEmployeesFrom = shiftsToShuffle.stream()
                    .min(Comparator.comparingInt(Shift::getFactorRequiredAvailable))
                    .orElse(null);

//            Shift shiftToRemoveEmployeesFrom = shiftsToShuffle.get(0);
            approvedShifts.add(shiftToRemoveEmployeesFrom);

            assert shiftToRemoveEmployeesFrom != null;
            removeEmployeesFromShift(shiftToRemoveEmployeesFrom);
            updateShiftSetAfterFulfilledOne(shiftToRemoveEmployeesFrom);
            findAndApproveShiftsWithFactorLessOrEqualToZero();
        }

        fulfillRosterByWeeks();
        tryToReplaceemployeesTofullFillRequirements();
    }

    private void fulfillRosterByWeeks() {
        Set<Integer> weeks = shiftSet.stream()
                .map(Shift::getWeekNumber)
                .distinct()
                .collect(Collectors.toSet());


        for (Integer week : weeks) {
            Set<Shift> shiftsFromWeek = shiftSet.stream()
                    .filter(s -> s.getWeekNumber() == week)
                    .collect(Collectors.toCollection(TreeSet::new));

            Set<Employee> freeEmployees = employeesHoursMap.keySet().stream()
                    .filter(e -> hoursInWeekForEmployee(e, shiftsFromWeek) < 40 - EmployeeHoursInWeek.getNumbersOfHOursForEmployeInWeek(e, week))
                    .collect(Collectors.toSet());

            for (Employee freeEmployee : freeEmployees) {
                while (hoursInWeekForEmployee(freeEmployee, shiftsFromWeek) < 40 - EmployeeHoursInWeek.getNumbersOfHOursForEmployeInWeek(freeEmployee, week)) {
                    Shift best = shiftsOKforEmployee(shiftsFromWeek, freeEmployee, week).stream()
                            .min(Comparator.comparingInt(Shift::getFactorRequiredAvailable))
                            .get();
                    best.addEmployee(freeEmployee);
                }
            }


        }
    }

    private int hoursInWeekForEmployee(Employee employee, Set<Shift> shifts) {
        return shifts.stream()
                .filter(s -> s.getEmployeeList().contains(employee))
                .mapToInt(Shift::getDuration)
                .sum();
    }

    private Set<Shift> shiftsOKforEmployee(Set<Shift> shifts, Employee employee, int week) {
        Set<Shift> employeeWorks = shifts.stream()
                .filter(s -> s.getEmployeeList().contains(employee))
                .collect(Collectors.toSet());

        Set<Shift> employeeDoesNotWork = shifts.stream()
                .filter(s -> !s.getEmployeeList().contains(employee))
                .collect(Collectors.toSet());
        return employeeDoesNotWork.stream()
                .filter(s -> shiftISokWithOther(s, employeeWorks))
                .collect(Collectors.toSet());
    }

    private boolean shiftISokWithOther(Shift shift, Set<Shift> others) {
        return others.stream()
                .noneMatch(o -> shiftsAreCloserThan11Hours(o, shift)) &&
                others.stream()
                        .noneMatch(o -> o.getStartTime().toLocalDate().equals(shift.getStartTime().toLocalDate()));
    }

    /**
     * Method check how flexible employee is. It means that method count employees occurrences in not approved shifts.
     **/
    private int getNumberOfNotApprovedShiftsWithEmployee(Employee employee) {
        return (int) shiftSet.stream()
                .filter(shift -> !approvedShifts.contains(shift))
                .filter(shift -> shift.getEmployeeList().contains(employee))
                .count();
    }

    private void removeEmployeesFromShift(Shift shift) {
        // Java sort method use MergeSort so first step is to shuffle employees list and then sort it using stable
        // sorting method which is MergeSort
        List<Employee> allEmployeesList = new ArrayList<>(shift.getEmployeeList());
        Collections.shuffle(allEmployeesList);
        allEmployeesList.sort(Comparator.comparingInt(this::getNumberOfNotApprovedShiftsWithEmployee));
        // then we can choose employees to stay in this shift and update hours map
        List<Employee> employeesToStay = allEmployeesList.stream()
                .limit(shift.getRequiredNumberOfEmployees())
                .collect(Collectors.toList());
        for (Employee employee : employeesToStay) {
            updateEmployeeHoursMap(employee, shift.getDuration());
        }
        // remove staying employees from list
        allEmployeesList.removeAll(employeesToStay);
        // then remove employees from shift
        allEmployeesList.forEach(shift::removeEmployee);
        // and we need to update other shifts as this is now done and fulfilled with employees
    }


    private void updateShiftSetAfterFulfilledOne(Shift approvedShift) {

        Set<Employee> employeesFromApprovedShift = approvedShift.getEmployeeList();

        Set<Shift> notApprovedShifts = shiftSet.stream()
                .filter(shift1 -> !approvedShifts.contains(shift1))
                .collect(Collectors.toSet());

        Set<Shift> shiftsCloserThan11Hours = notApprovedShifts.stream()
                .filter(shift -> shiftsAreCloserThan11Hours(approvedShift, shift))
                .collect(Collectors.toSet());

        for (Employee employee : employeesFromApprovedShift) {
            for (Shift shiftsCloserThan11Hour : shiftsCloserThan11Hours) {
                shiftsCloserThan11Hour.removeEmployee(employee);
            }
        }

        Set<Shift> shiftsFromSameDay = notApprovedShifts.stream()
                .filter(shift -> shift.getStartTime().toLocalDate().equals(approvedShift.getStartTime().toLocalDate()))
                .collect(Collectors.toSet());

        for (Employee employee : employeesFromApprovedShift) {
            for (Shift shift : shiftsFromSameDay) {
                shift.removeEmployee(employee);
            }
        }

        Set<Shift> shiftsFromSameWeek = notApprovedShifts.stream()
                .filter(shift -> shift.getWeekNumber() == approvedShift.getWeekNumber())
                .collect(Collectors.toSet());

        for (Employee employee : employeesFromApprovedShift) {
            dealWithSameWeekShifts(shiftsFromSameWeek, employee);
        }

    }

    /**
     * Method check if the set passed as argument is not empty.
     * If there is at least one shift , method calculate number of working hours
     * for employee in this week. If the number of working hours is more or equal to 40
     * employee is removed from all not approved shift in this week.
     *
     * @param shiftsFromSameWeek nor approved shifts
     * @param employee           employee which method checks
     */
    private void dealWithSameWeekShifts(Set<Shift> shiftsFromSameWeek, Employee employee) {
        if (shiftsFromSameWeek.size() > 0) {
            int weekNumber = shiftsFromSameWeek.stream().findAny().get().getWeekNumber();
            Set<Shift> approvedShitForEmployeeInThisWeek = approvedShifts.stream()
                    .filter(shift -> shift.getEmployeeList().contains(employee))
                    .filter(shift -> shift.getWeekNumber() == weekNumber)
                    .collect(Collectors.toSet());
            int numberOfWorkingHoursOfEmployeeForThisWeek = approvedShitForEmployeeInThisWeek.stream()
                    .map(Shift::getDuration)
                    .reduce((a, b) -> a + b)
                    .orElse(0);
            if (numberOfWorkingHoursOfEmployeeForThisWeek >= 40 - EmployeeHoursInWeek.getNumbersOfHOursForEmployeInWeek(employee, weekNumber)) {
                for (Shift shift : shiftsFromSameWeek) {
                    shift.removeEmployee(employee);
                }
            }
        }
    }


    public static boolean shiftsAreCloserThan11Hours(Shift approved, Shift toCheck) {

        LocalDateTime start = approved.getStartTime().minusHours(11);
        LocalDateTime end = approved.getEndTime().plusHours(11);

        return toCheck.getEndTime().isAfter(start) && !toCheck.getEndTime().isAfter(end)
                ||
                !toCheck.getStartTime().isBefore(start) && toCheck.getEndTime().isBefore(end);
    }


    // check if any shift still has got more employees than it need
    private boolean checkIfItIsPossibleToRemoveEmployeesFromShifts() {
        return shiftSet.stream()
                .anyMatch(shift -> shift.getFactorRequiredAvailable() > 0);
    }

    /**
     * It is just simple method  to update hours map for employees.
     * Each employee has got a maximum number of hours he can spend working.
     *
     * @param employee is the employee approved for shift
     * @param duration is the duration of the shift where employee approved is
     */
    private void updateEmployeeHoursMap(Employee employee, int duration) {
        employeesHoursMap.put(employee, employeesHoursMap.getOrDefault(employee, 0) + duration);
    }

    /**
     * As long as there is any shift with factor <= 0 , methods approves this shift.
     * After approval method calls method which update other not approved shifts.
     * It works recursively while all shifts are approved or there is more than required
     * number of employees in each not approved shift.
     **/
    private void findAndApproveShiftsWithFactorLessOrEqualToZero() {
        while (shiftSet.stream().filter(s -> !approvedShifts.contains(s)).anyMatch(shift -> shift.getFactorRequiredAvailable() <= 0)) {

            List<Shift> shiftsToShuffle = shiftSet.stream()
                    .filter(s -> !approvedShifts.contains(s))
                    .filter(shift -> shift.getFactorRequiredAvailable() <= 0)
                    .collect(Collectors.toList());

            Collections.shuffle(shiftsToShuffle);
            shiftsToShuffle.sort(Comparator.comparingInt(Shift::getFactorRequiredAvailable));

            Shift shiftToApprove = shiftsToShuffle.stream()
                    .findFirst()
                    .orElse(null);

            assert shiftToApprove != null;
            approvedShifts.add(shiftToApprove);
            removeEmployeesFromShift(shiftToApprove);
            updateShiftSetAfterFulfilledOne(shiftToApprove);
        }
    }

    @Override
    public String toString() {
        System.out.println("-----------------------");
        StringBuilder sb = new StringBuilder("--ROSTER--");
        sb.append("\n");
        for (Shift approvedShift : approvedShifts) {
            sb.append(approvedShift.toString());
            sb.append('\n');
        }
        return sb.toString();
    }

    public void displayForEmployees() {
        Set<Employee> employees = new TreeSet<>();

        for (Shift shift : shiftSet) {
            employees.addAll(shift.getEmployeeList());
        }

        for (Employee employee : employees) {
            Set<Shift> shifts = shiftSet.stream()
                    .filter(s -> s.getEmployeeList().contains(employee))
                    .collect(Collectors.toSet());
            System.out.println(new RosterForEmployee(shifts, employee, shiftSet.size()));
        }

    }

    public void displayShiftStats() {
        int days = (int) shiftSet.stream()
                .filter(s -> s.getEmployeeList().size() < s.getRequiredNumberOfEmployees())
                .count();

        Set<Shift> shifts = shiftSet.stream()
                .filter(s -> s.getEmployeeList().size() < s.getRequiredNumberOfEmployees())
                .collect(Collectors.toSet());

//        for (Shift shift : shiftSet) {
//            System.out.println(shift.getEmployeeList().size() + " / "+ shift.getRequiredNumberOfEmployees());
//        }

        System.out.println(days + " / " + shiftSet.size());

        for (Shift shift : shifts) {
            System.out.println(shift.getEmployeeList().size() + " / " + shift.getRequiredNumberOfEmployees());
        }
    }

    public Set<Shift> getShiftSet() {
        return Collections.unmodifiableSet(shiftSet);
    }

    public void tryToReplaceemployeesTofullFillRequirements() {
        Set<Integer> weeks = shiftSet.stream()
                .map(Shift::getWeekNumber)
                .distinct()
                .collect(Collectors.toSet());

        for (Integer week : weeks) {
            Set<Shift> fromWeek = shiftSet.stream()
                    .filter(s -> s.getWeekNumber() == week)
                    .collect(Collectors.toCollection(TreeSet::new));
            tryToReplaceShiftsInOneWeekToFullfilRequirements(fromWeek);
        }
    }

    private void tryToReplaceShiftsInOneWeekToFullfilRequirements(Set<Shift> shiftsFromOneWeek) {
        int counter = 0;
        while (shiftsFromOneWeek.stream()
                .anyMatch(s->s.getEmployeeList().size()<s.getRequiredNumberOfEmployees())){

            counter++;
            if (counter>=1000*shiftsFromOneWeek.size())return;

            Set<Shift> notEnough = shiftsFromOneWeek.stream()
                    .filter(s -> s.getEmployeeList().size() < s.getRequiredNumberOfEmployees())
                    .collect(Collectors.toCollection(TreeSet::new));
            Set<Shift> toMany = shiftsFromOneWeek.stream()
                    .filter(s -> s.getEmployeeList().size() > s.getRequiredNumberOfEmployees())
                    .collect(Collectors.toCollection(TreeSet::new));


            for (Shift notE : notEnough) {
                for (Shift toM : toMany) {

                    List<Employee> fromNotEnough = notE.getEmployeeList().stream()
                            .distinct()
                            .collect(Collectors.toCollection(LinkedList::new));

                    List<Employee> fromToMany = toM.getEmployeeList().stream()
                            .distinct()
                            .collect(Collectors.toCollection(LinkedList::new));

                    List<Employee> potentiallEmployees = fromToMany.stream()
                            .filter(e -> !fromNotEnough.contains(e))
                            .collect(Collectors.toCollection(LinkedList::new));

                    for (Employee potentiallEmployee : potentiallEmployees) {

                        toM.removeEmployee(potentiallEmployee);
                        Set<Shift> employeeShifts = shiftsFromOneWeek.stream()
                                .filter(s->s.getEmployeeList().contains(potentiallEmployee))
                                .collect(Collectors.toCollection(TreeSet::new));
                        if(shiftISokWithOther(notE, employeeShifts)){
                            notE.addEmployee(potentiallEmployee);
                            break;
                        }else{
                            toM.addEmployee(potentiallEmployee);
                        }
                    }

                }
            }
        }
    }
}
