package Roster;

import Employees.Employee;
import Preferences.RosterForEmployeePricingClass;
import Shifts.Shift;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

public class RosterForEmployee {
    private Set<Shift> employeeShifts = new TreeSet<>();
    private Employee employee;
    private int maxRate;

    public RosterForEmployee(Set<Shift> employeeShifts, Employee employee, int maxRate) {
        this.employeeShifts.addAll(employeeShifts);
        this.employee = employee;
        this.maxRate = maxRate;
    }

    public int getRosterRate() {
        return maxRate - getNumberOfNightShifts() * RosterForEmployeePricingClass.getWorkingNightFine()
                - getNumberOfWorkinSundays() * RosterForEmployeePricingClass.getWorkingSundayFine()
                - getNumberOfWorkinSaturdays() * RosterForEmployeePricingClass.getWorkingSaturdayFine()
                - getNumberOfWorkingWeekends() * RosterForEmployeePricingClass.getWorkingWeekendFine()
                - getNumberOFSingleFreeDay() * RosterForEmployeePricingClass.getOneFreeDayInRowFine();
    }

    public int getNumberOfWorkinSundays() {
        return (int) employeeShifts.stream()
                .filter(sh -> sh.getStartTime().getDayOfWeek().equals(DayOfWeek.SUNDAY))
                .count();
    }

    public int getNumberOfWorkinSaturdays() {
        return (int) employeeShifts.stream()
                .filter(sh -> sh.getStartTime().getDayOfWeek().equals(DayOfWeek.SATURDAY))
                .count();
    }

    public int getNumberOfNightShifts() {
        return (int) employeeShifts.stream()
                .filter(s -> !s.getStartTime().toLocalTime().isBefore(LocalTime.of(21, 0)) &&
                        !s.getEndTime().toLocalTime().isAfter(LocalTime.of(7, 0)))
                .count();
    }

    public int getNumberOfWorkingWeekends() {
        Set<LocalDate> sundays = employeeShifts.stream()
                .filter(s -> s.getStartTime().getDayOfWeek().equals(DayOfWeek.SUNDAY))
                .map(s -> s.getStartTime().toLocalDate())
                .collect(Collectors.toSet());
        Set<LocalDate> saturdays = employeeShifts.stream()
                .filter(s -> s.getStartTime().getDayOfWeek().equals(DayOfWeek.SATURDAY))
                .map(s -> s.getStartTime().toLocalDate())
                .collect(Collectors.toSet());

        return (int) saturdays.stream()
                .filter(sat -> sundays.contains(sat.plusDays(1)))
                .count();
    }

    public int getNumberOfWorkingHours() {
        return employeeShifts.stream()
                .map(s -> s.getDuration())
                .reduce((a, b) -> a + b)
                .orElse(0);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("__IND__ROSTER__" + employee.getName() + "__" + employee.getJobTime() + "__" + getNumberOfWorkingHours());
        sb.append('\n');
        for (Shift shift : employeeShifts) {
            String singleShift = shift.getStartTime() + " " + shift.getEndTime() + " " + shift.getStartTime().getDayOfWeek();
            sb.append(singleShift);
            sb.append('\n');
        }
        return sb.toString();
    }

    public int getNumberOFSingleFreeDay() {
        List<LocalDate> workingDays = employeeShifts.stream()
                .map(s -> s.getStartTime().toLocalDate())
                .sorted()
                .collect(Collectors.toCollection(ArrayList::new));

        int sum = 0;

        for (int i = 0; i < workingDays.size() - 1; i++) {
            if(workingDays.get(i).plusDays(2).equals(workingDays.get(i+1))){
                sum++;
            }
        }
        return sum;
    }

    public int getFreeSunMon(){
        List<Integer> weeks = employeeShifts.stream()
                .map(Shift::getWeekNumber)
                .distinct()
                .sorted()
                .collect(Collectors.toCollection(ArrayList::new));

        int sum = 0;

        for(int i=0; i<weeks.size()-1;i++){
            int finalI = i;
            List<DayOfWeek> daysFirst = employeeShifts.stream()
                    .filter(s->s.getWeekNumber()==weeks.get(finalI))
                    .map(s->s.getStartTime().getDayOfWeek())
                    .collect(Collectors.toCollection(LinkedList::new));

            List<DayOfWeek> daysSecond = employeeShifts.stream()
                    .filter(s->s.getWeekNumber()==weeks.get(finalI+1))
                    .map(s->s.getStartTime().getDayOfWeek())
                    .collect(Collectors.toCollection(LinkedList::new));

            if(!daysFirst.contains(DayOfWeek.SUNDAY) && !daysSecond.contains(DayOfWeek.MONDAY)){
                sum++;
            }
        }

        return sum;
    }
}
