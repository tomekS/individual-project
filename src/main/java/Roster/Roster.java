package Roster;

import Employees.Employee;
import Shifts.Shift;
import com.sun.javafx.scene.control.skin.VirtualFlow;


import java.util.*;
import java.util.stream.Collectors;

public class Roster {
    private Set<Shift> shiftSet;
    private Set<Employee> employees;
    private List<RosterForEmployee> rosterForEmployees = new LinkedList<>();

    public Roster(Set<Shift> shiftSet, Set<Employee> employees) {
        this.shiftSet = shiftSet;
        this.employees = employees;
        generate();
    }

    @Override
    public String toString() {
        System.out.println("-----------------------");
        StringBuilder sb = new StringBuilder("--ROSTER--");
        sb.append("\n");
        Set<Shift> notFull = shiftSet.stream()
                .filter(s -> s.getEmployeeList().size() < s.getRequiredNumberOfEmployees())
                .collect(Collectors.toSet());
        for (Shift approvedShift : notFull) {
            sb.append(approvedShift.toString());
            sb.append('\n');
        }
        return sb.toString();
    }

    public void displayShiftStats(){
        int days = (int)shiftSet.stream()
                .filter(s -> s.getEmployeeList().size() < s.getRequiredNumberOfEmployees())
                .count();

        Set<Shift> shifts = shiftSet.stream()
                .filter(s -> s.getEmployeeList().size() < s.getRequiredNumberOfEmployees())
                .collect(Collectors.toSet());

//        for (Shift shift : shiftSet) {
//            System.out.println(shift.getEmployeeList().size() + " / "+ shift.getRequiredNumberOfEmployees());
//        }

        System.out.println(days+ " / "+shiftSet.size());

    }

    public boolean isFullfilled(){
        return shiftSet.stream()
                .allMatch(s -> s.getRequiredNumberOfEmployees() <= s.getEmployeeList().size());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Roster roster = (Roster) o;
        List<Shift> thoseShifts = new LinkedList<>(shiftSet);
        List<Shift> rosterShifts = new LinkedList<>(roster.shiftSet);
        return thoseShifts.containsAll(rosterShifts) && rosterShifts.containsAll(thoseShifts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(shiftSet);
    }

    private void generate(){
        for (Employee employee : employees) {
            Set<Shift> shifts = shiftSet.stream()
                    .filter(s -> s.getEmployeeList().contains(employee))
                    .collect(Collectors.toSet());
            rosterForEmployees.add(new RosterForEmployee(shifts, employee, shiftSet.size()));
        }
    }

    public double getFulfilledRate(){
        return shiftSet.stream()
                .filter(s -> isFullfilled())
                .count() / shiftSet.size();
    }

    public double getRate(){

        List<Integer> rates = rosterForEmployees.stream()
                .map(RosterForEmployee::getRosterRate)
                .collect(Collectors.toList());

        double avv_rates = rates.stream()
                .mapToInt(i ->i)
                .average()
                .orElse(0);

        double empl_ros_sum = rates.stream()
                .reduce((a,b)->a+b)
                .orElse(Integer.MIN_VALUE) + getFulfilledRate()*100000;

        return empl_ros_sum - rates.stream()
                .mapToDouble(r -> Math.abs(avv_rates-r))
                .sum();
    }

//    public int getWorkingSundays(){
//        return rosterForEmployees.stream()
//                .map(RosterForEmployee::getNumberOfWorkinSundays)
//                .reduce((a,b)->a+b)
//                .orElse(0);
//    }
//
//    public int getWorkingSaturdays(){
//        return rosterForEmployees.stream()
//                .map(RosterForEmployee::getNumberOfWorkinSaturdays)
//                .reduce((a,b)->a+b)
//                .orElse(0);
//    }
//
//    public int getWorkingWeekends(){
//        return rosterForEmployees.stream()
//                .map(RosterForEmployee::getNumberOfWorkinSundays)
//                .reduce((a,b)->a+b)
//                .orElse(0);
//    }

    public List<RosterForEmployee> getRosterForEmployees(){
        return Collections.unmodifiableList(rosterForEmployees);
    }

    public void displayRate(){
        for (Shift shift : new TreeSet<>(shiftSet)) {
            System.out.println(shift.getStartTime().toLocalDate()+ " " + shift.getStartTime().getDayOfWeek() + " " + shift.getEmployeeList().size()+"/"+shift.getRequiredNumberOfEmployees());
        }
    }

    public void displayOcadoStats(){
        for (RosterForEmployee rosterForEmployee : rosterForEmployees) {
            int numOfWeeks = (int)shiftSet.stream().mapToInt(Shift::getWeekNumber).distinct().count();
            int freeSats = numOfWeeks - rosterForEmployee.getNumberOfWorkinSaturdays();
            int freeWeekEnds = numOfWeeks - rosterForEmployee.getNumberOfWorkingWeekends();
            int workingSun = rosterForEmployee.getNumberOfWorkinSundays();
            int sunMon = rosterForEmployee.getFreeSunMon();
            System.out.println(freeSats+","+freeWeekEnds+","+workingSun+","+sunMon);
        }
    }
}
