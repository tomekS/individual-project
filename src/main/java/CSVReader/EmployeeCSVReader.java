package CSVReader;

import Employees.BasicEmployee;
import Employees.Employee;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeeCSVReader {
    public static List<Employee> readEmployeesFromFile(String pathToFile) throws IllegalArgumentException {

        List<String> lines;

        try (Stream<String> stream = Files.lines(Paths.get(pathToFile))) {
            lines = stream.collect(Collectors.toList());
        }catch (Exception e){
            throw new IllegalArgumentException("CSV employees reader ERROR");
        }

        return lines.stream()
                .map(EmployeeCSVReader::getEmployeeFromString)
                .collect(Collectors.toList());
    }

    private static Employee getEmployeeFromString(String lineWithEmployee) {
        String[] params = lineWithEmployee.split(",");
        String name = params[0].trim();
        int id = Integer.valueOf(params[1].trim());
        double timePerWeek = Double.valueOf(params[2].trim());

        return new BasicEmployee(name, id, timePerWeek);
    }
}
