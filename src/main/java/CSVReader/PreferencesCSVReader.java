package CSVReader;

import Preferences.Preference;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PreferencesCSVReader {
    public static List<Preference> readPreferencesFromFile(String pathToFile) throws IllegalArgumentException {

        List<String> lines;

        try (Stream<String> stream = Files.lines(Paths.get(pathToFile))) {
            lines = stream.collect(Collectors.toList());
        }catch (Exception e){
            throw new IllegalArgumentException("CSV preferences reader ERROR");
        }

        return lines.stream()
                .map(PreferencesCSVReader::getPreferenceFromLine)
                .collect(Collectors.toList());
    }

    private static Preference getPreferenceFromLine(String lineWithEmployee) {
        String[] params = lineWithEmployee.split(";");

        String startDateString = params[1];
        String startHourString = params[2];

        String endDateString = params[3];
        String endHourString = params[4];

        int employeeId = Integer.parseInt(params[0]);

        LocalDate startDate = LocalDate.parse(startDateString);
        LocalDate endDate = LocalDate.parse(endDateString);
        LocalTime startTime = LocalTime.parse(startHourString);
        LocalTime endTime = LocalTime.parse(endHourString);

        LocalDateTime startLocalDateTime = LocalDateTime.of(startDate, startTime);
        LocalDateTime endLocalDateTime = LocalDateTime.of(endDate, endTime);

        return new Preference(employeeId, startLocalDateTime, endLocalDateTime);
    }
}
