package CSVReader;

import Shifts.Shift;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RequirementCSVReader {
    public static List<Shift> readRequirementsFromFile(String pathToFile) throws IllegalArgumentException {

        List<String> lines;

        try (Stream<String> stream = Files.lines(Paths.get(pathToFile))) {
            lines = stream.collect(Collectors.toList());
        } catch (Exception e) {
            throw new IllegalArgumentException("CSV requirements reader ERROR");
        }

        return lines.stream()
                .map(RequirementCSVReader::getShiftFromString)
                .collect(Collectors.toList());
    }

    private static Shift getShiftFromString(String lineWithShift) {
        String[] params = lineWithShift.split(";");

        String startDateString = params[0];
        String startHourString = params[1];

        String endDateString = params[2];
        String endHourString = params[3];

        int requiredNumberOfEmployees = Integer.parseInt(params[4]);

        LocalDate startDate = LocalDate.parse(startDateString);
        LocalDate endDate = LocalDate.parse(endDateString);
        LocalTime startTime = LocalTime.parse(startHourString);
        LocalTime endTime = LocalTime.parse(endHourString);

        LocalDateTime startLocalDateTime = LocalDateTime.of(startDate, startTime);
        LocalDateTime endLocalDateTime = LocalDateTime.of(endDate, endTime);


        return new Shift(startLocalDateTime, endLocalDateTime, requiredNumberOfEmployees);
    }
}
