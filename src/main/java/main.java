//import Builders.MultiThreadEngine;
//import Builders.RosterBuilder;
//import Development.DataSupplier;
//import Employees.BasicEmployee;
//import Employees.Employee;
//import Shifts.Shift;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//import sun.util.resources.LocaleData;
//
//import java.io.FileNotFoundException;
//import java.lang.reflect.Type;
//import java.time.Duration;
//import java.time.LocalDateTime;
//import java.time.LocalTime;
//import java.time.temporal.WeekFields;
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Locale;
//import java.util.concurrent.ExecutionException;
//
//
//public class main {
//    public static void main(String[] args) throws FileNotFoundException, ExecutionException, InterruptedException {
//        MultiThreadEngine engine = new MultiThreadEngine(DataSupplier.getOcadoTestEmployees(),
//                DataSupplier.getOcadoShifts(), DataSupplier.getTestPreferences(), 8, 1000);
//        LocalTime start = LocalTime.now();
//        engine.generate();
//        LocalTime end = LocalTime.now();
//        System.out.println(Duration.between(start, end).getSeconds());
//        System.out.println(Runtime.getRuntime().availableProcessors());
//    }
//}
