package Builders;

import Employees.Employee;
import Preferences.EmployeeHoursInWeek;
import Preferences.Preference;
import Roster.FirstStageRoster;
import Roster.Roster;
import Roster.SecondStageRoster;
import Roster.ThirdStageRoster;
import Shifts.Shift;
import Writers.RosterWriter;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

public class RosterBuilder implements Callable<List<Roster>> {
    private Set<Employee> employees;
    private Set<Shift> shifts;
    private Set<Preference> preferences;
    private FirstStageRoster firstStageRoster;
    private SecondStageRoster secondStageRoster;
    private ThirdStageRoster thirdStageRoster;
    private String workingDirectory;
    private int number;

    public RosterBuilder(Set<Employee> employees, Set<Shift> shifts, Set<Preference> preferences, String workingDirectory,
                         int number) {

        this.employees = employees;
        this.shifts = shifts;
        this.preferences = preferences;
        this.workingDirectory = workingDirectory;
        this.number = number;
    }

    private void createFirstStageRoster() {
        this.firstStageRoster = new FirstStageRoster(employees, shifts);
    }

    private void crateSecondStage() {
        this.secondStageRoster = new SecondStageRoster(firstStageRoster, preferences);
    }

    private void createThirdStageRoster() {
        this.thirdStageRoster = new ThirdStageRoster(this.secondStageRoster.getShiftSet());
    }

    public FirstStageRoster getFirstStageRoster() {
        return firstStageRoster;
    }

    public SecondStageRoster getSecondStageRoster() {
        return secondStageRoster;
    }

    @Override
    public List<Roster> call() throws Exception {
        EmployeeHoursInWeek.fulFillMap(employees, preferences);
        createFirstStageRoster();
        crateSecondStage();
        createThirdStageRoster();

        LinkedList<Roster> roster = new LinkedList<>();
        roster.add(new Roster(thirdStageRoster.getShiftSet(), employees));
        new RosterWriter(workingDirectory, roster.getFirst(), "tmp_roster_" + number).run();
        return roster;
    }
}
