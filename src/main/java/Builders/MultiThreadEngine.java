package Builders;

import Employees.Employee;
import Preferences.Preference;
import Roster.Roster;
import Roster.RosterForEmployee;
import Shifts.Shift;
import Writers.RosterWriter;

import java.io.File;
import java.nio.file.Path;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class MultiThreadEngine {
    private Set<Employee> employees;
    private Set<Shift> shifts;
    private Set<Preference> preferences;
    private ExecutorService executorService;
    private int sizeOfPopulation;
    private Path workingDirectory;

    public MultiThreadEngine(Set<Employee> employees, Set<Shift> shifts, Set<Preference> preferences,
                             int numberOfThreads, int sizeOfPopulation, Path workingDirectory) {

        this.employees = employees;
        this.shifts = shifts;
        this.preferences = preferences;
        this.executorService = Executors.newFixedThreadPool(numberOfThreads);
        this.sizeOfPopulation = sizeOfPopulation;
        this.workingDirectory = workingDirectory;
    }

    private Set<Shift> getCopyOfShifts() {
        return shifts.stream()
                .map(Shift::copy)
                .collect(Collectors.toSet());
    }

    public void generate() throws InterruptedException, ExecutionException {
        List<RosterBuilder> builders = new LinkedList<>();
        for (int i = 0; i < sizeOfPopulation; i++) {
            builders.add(new RosterBuilder(employees, getCopyOfShifts(), preferences,
                    workingDirectory.toString() + File.separator + "tmp", i + 1));
        }
        List<Future<List<Roster>>> rosterList = executorService.invokeAll(builders);
        List<Roster> rosters = new LinkedList<>();
        executorService.shutdown();
        for (Future<List<Roster>> listFuture : rosterList) {
            System.out.println("beka" + LocalTime.now());
            Optional<Roster> any = listFuture.get().stream()
                    .findAny();
            if (any.isPresent()) {
                rosters.add(any.get());
            }
        }

        List<Roster> full = rosters.stream()
                .filter(Roster::isFullfilled)
                .collect(Collectors.toCollection(LinkedList::new));

        full.forEach(Roster::displayShiftStats);

        System.out.println("Fullfilled: " + rosters.stream()
                .filter(Roster::isFullfilled)
                .count());
        System.out.println("Distinct: " + rosters.stream().distinct().count());
        Roster best = rosters.stream()
                .max(Comparator.comparingDouble(Roster::getRate))
                .get();

        List<Roster> bestEight = rosters.stream()
                .sorted(Comparator.comparingDouble(Roster::getRate).reversed())
                .limit(8)
                .collect(Collectors.toCollection(LinkedList::new));


        int i = 0;
        for (Roster roster : bestEight) {
            System.out.println("-----------------");
            roster.displayOcadoStats();
            new RosterWriter(workingDirectory.toString() + File.separator + "best", roster,
                    "best_" + ++i).run();
        }

        System.out.println("best---------");
        for (RosterForEmployee rosterForEmployee : best.getRosterForEmployees()) {
            System.out.println(rosterForEmployee);
        }
        best.displayOcadoStats();
    }

    private void displayRosterStats(List<RosterForEmployee> forEmployees) {
        for (RosterForEmployee forEmployee : forEmployees) {
            System.out.println(forEmployee.getNumberOfNightShifts() + " "
                    + forEmployee.getNumberOfWorkingWeekends() + " "
                    + forEmployee.getNumberOfWorkinSundays() + " "
                    + forEmployee.getNumberOfWorkingHours() + " "
                    + forEmployee.getNumberOFSingleFreeDay());
        }
    }
}
