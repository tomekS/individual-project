package Shifts;

import Employees.Employee;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.WeekFields;
import java.util.*;

public class Shift implements Comparable<Shift> {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private int requiredNumberOfEmployees;
    private Set<Employee> employeeList = new HashSet<>();
    private Set<Employee> fromPreferences = new TreeSet<>();

    public Shift(LocalDateTime startTime, LocalDateTime endTime, int requiredNumberOfEmployees) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.requiredNumberOfEmployees = requiredNumberOfEmployees;
    }

    private Shift(Shift shiftToCopyFromIt) {
        this.startTime = shiftToCopyFromIt.getStartTime();
        this.endTime = shiftToCopyFromIt.getEndTime();
        this.requiredNumberOfEmployees = shiftToCopyFromIt.getRequiredNumberOfEmployees();
        this.employeeList.addAll(shiftToCopyFromIt.getEmployeeList());
    }

    public int getRequiredNumberOfEmployees() {
        return requiredNumberOfEmployees;
    }

    public int getFactorRequiredAvailable() {
        return employeeList.size() - this.getRequiredNumberOfEmployees();
    }

    public void addEmployee(Employee employee) {
        employeeList.add(employee);
    }

    public void removeEmployee(Employee employee) {
        employeeList.remove(employee);
    }

    public void removeEmployee(int employeeID) {
        employeeList.stream().filter(emp -> emp.getId() == employeeID)
                .findAny()
                .ifPresent(employee1 -> employeeList.remove(employee1));
    }

    public Set<Employee> getEmployeeList() {
        return Collections.unmodifiableSet(employeeList);
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    @Override
    public int compareTo(Shift shift) {
        return startTime.compareTo(shift.startTime);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("__Shift__");
        sb.append(getStartTime() + " " + getEndTime());
        sb.append('\n');
        for (Employee employee : employeeList) {
            sb.append(employee.toString());
            sb.append('\n');
        }
        return sb.toString();
    }

    public Shift copy() {
        return new Shift(this);
    }

    public int getDuration() {
        return (int) Duration.between(getStartTime(), getEndTime()).toHours();
    }

    public int getWeekNumber() {
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        return this.startTime.get(weekFields.weekOfWeekBasedYear());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        Shift toCompare = (Shift) o;
        if (!startTime.equals(toCompare.getStartTime())) return false;
        if (!endTime.equals(toCompare.getEndTime())) return false;
        if (requiredNumberOfEmployees != toCompare.requiredNumberOfEmployees) return false;
        List<Employee> shiftEmployees = new LinkedList<>(this.employeeList);
        List<Employee> toCompareEmployees = new LinkedList<>(toCompare.employeeList);
        return shiftEmployees.containsAll(toCompareEmployees) && toCompareEmployees.containsAll(shiftEmployees);
    }

    @Override
    public int hashCode() {

        return Objects.hash(startTime, endTime, requiredNumberOfEmployees, employeeList);
    }
}
