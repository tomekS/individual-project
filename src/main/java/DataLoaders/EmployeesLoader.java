package DataLoaders;

import Employees.BasicEmployee;
import Employees.Employee;
import com.google.gson.Gson;

public class EmployeesLoader {
    public static Employee getFullTimeEmployeeFromJSON(String json){
        return new Gson().fromJson(json, BasicEmployee.class);
    }
}
