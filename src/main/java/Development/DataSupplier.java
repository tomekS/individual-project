package Development;

import Employees.BasicEmployee;
import Employees.Employee;
import Preferences.Preference;
import Shifts.Shift;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.MonthDay;
import java.time.temporal.WeekFields;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class DataSupplier {
    public static Set<Employee> getOcadoTestEmployees(){
        Set<Employee> result = new TreeSet<>();

        result.add(new BasicEmployee("Jan", 1,40));
        result.add(new BasicEmployee("Zbyszek", 2,40));
        result.add(new BasicEmployee("Tomek", 3,40));
        result.add(new BasicEmployee("Jacek", 4,40));
        result.add(new BasicEmployee("Przemek", 5,40));
        result.add(new BasicEmployee("Tytus", 6,40));
        result.add(new BasicEmployee("Seba", 7,40));
        result.add(new BasicEmployee("Gienek", 8,40));
        result.add(new BasicEmployee("Staszek", 9,40));
        result.add(new BasicEmployee("Wania", 10,40));

        return result;
    }

    public static Set<Employee> getTestEmployees(){
        Set<Employee> result = new TreeSet<>();

        result.add(new BasicEmployee("Jan", 1,40));
        result.add(new BasicEmployee("Zbyszek", 2,168));
        result.add(new BasicEmployee("Tomek", 3,168));
        result.add(new BasicEmployee("Jacek", 4,168));
        result.add(new BasicEmployee("Przemek", 5,168));
        result.add(new BasicEmployee("Tytus", 6,168));
        result.add(new BasicEmployee("Seba", 7,168));
        result.add(new BasicEmployee("Gienek", 8,168));
        result.add(new BasicEmployee("Staszek", 9,168));
        result.add(new BasicEmployee("Wania", 10,168));

        return result;
    }

    public static Set<Shift> getTestShifts(){
        Set<Shift> result = new TreeSet<>();

        LocalDateTime firstShiftStart = LocalDateTime.of(2018, 1, 1, 6,0);
        LocalDateTime firstShiftEnd = LocalDateTime.of(2018, 1, 1, 14,0);


        LocalDateTime secondShiftStart = LocalDateTime.of(2018, 1, 1, 14,0);
        LocalDateTime secondShiftEnd = LocalDateTime.of(2018, 1, 1, 22,0);

        for(int i=0; i<28; i++){
            result.add(new Shift(firstShiftStart.plusDays(i),firstShiftEnd.plusDays(i),3));
            result.add(new Shift(secondShiftStart.plusDays(i),secondShiftEnd.plusDays(i),3));
        }

        return result;
    }

    public static Set<Shift> getOcadoShifts(){
        Set<Shift> result = new TreeSet<>();

        LocalDateTime firstShiftStart = LocalDateTime.of(2011, 8, 29, 6,0);
        LocalDateTime firstShiftEnd = LocalDateTime.of(2011, 8, 29, 14,0);


        for(int i=0; i<91; i++){
            if(firstShiftStart.plusDays(i).getDayOfWeek() == DayOfWeek.SUNDAY){
                result.add(new Shift(firstShiftStart.plusDays(i),firstShiftEnd.plusDays(i),8));
            }else{
                result.add(new Shift(firstShiftStart.plusDays(i),firstShiftEnd.plusDays(i),7));
            }
        }

        return result;
    }

    public static Set<Preference> getTestPreferences(){
        Set<Preference> result = new TreeSet<>();
//        result.add(new Preference(1, LocalDateTime.of(2017, 1, 3, 7,0),LocalDateTime.of(2017, 1, 3, 19,0)));
//        result.add(new Preference(5, LocalDateTime.of(2017, 1, 8, 7,0),LocalDateTime.of(2017, 1, 8, 19,0)));
//        result.add(new Preference(7, LocalDateTime.of(2017, 1, 23, 7,0),LocalDateTime.of(2017, 1, 23, 19,0)));
        return result;
    }
}
