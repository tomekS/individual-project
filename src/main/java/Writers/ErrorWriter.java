package Writers;

import java.io.*;

public class ErrorWriter {
    public static void writeErrorMessage(File errorFile, String message) throws IOException {
        FileWriter fileWriter = new FileWriter(errorFile, true);
        Writer writer = new BufferedWriter(fileWriter);
        writer.write(message);
        writer.write(System.lineSeparator());
        writer.close();
    }
}
