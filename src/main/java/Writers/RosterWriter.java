package Writers;

import Roster.Roster;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class RosterWriter implements Runnable {

    private String workingDirectory;
    private Roster roster;
    private String fileName;

    public RosterWriter(String workingDirectory, Roster roster, String fileName) {
        this.workingDirectory = workingDirectory;
        this.roster = roster;
        this.fileName = fileName;
    }

    @Override
    public void run() {
        try (Writer writer = new FileWriter(workingDirectory + File.separator + fileName + ".json")) {
            Gson gson = new GsonBuilder().create();
            gson.toJson(roster, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}